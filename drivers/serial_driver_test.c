//
//  serial_driver_test.c
//  MSA
//
//  Created by William Dillon on 4/30/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "driver.h"
#include "../analyzer/commander.h"
#include "../analyzer/hybrid_tuner.h"

int
main(void)
{
    driver_t *driver = NULL;
    if (serialCountPorts() > 0) {
        char **paths = serialListPortPaths();
        driver = newSerialDriver(1000000, paths[0]);
    } else {
        return 1;
    }
    
    if (driver == NULL) {
        fprintf(stderr, "Unable to create the RasPi driver.\n");
        return 1;
    }

    uint8_t *results;

    // Test loading a set of known values into the control board
    uint8_t testBuffer[] = "abcdefghijklmnopqrstuvwxyz";
    results = driver->command(driver, 26, (uint32_t *)testBuffer);
    free(results);
//    uint32_t value = 0xDEADBEEF;
//    results = driver->command(driver, 1, &value);
//    free(results);
    
    // Test loading an SPI transmission
//    command = newCommand();
//    initPins(0x00000000);
//    appendTransferSPI(command, 12, 16, 20, 16, 0xAA55);
//    insertTransferSPI(command,  0,  4,  8, 16, 0x55AA, 0);
//    results = driver->command(driver, command->length, command->array);
//    free(results);
    
    // Print a progress bar (assume 80 columns)
    printf("----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----|\n");
    
    // Generate 10000 fake tunings to banchmark scan rate
    hybrid_tuner_t *tuner = newHybridTuner(4118, 1);
    if (tuner == NULL) {
        fprintf(stderr, "Unable to initialize the tuner");
        return 1;
    }
    
    HTsetRefClock(tuner, 64000000);
    HTsetIdealDDSFreq(tuner, 10700000);
    HTsetIdealPhaseFreq(tuner, 972000);
    
    LMX_registers_t registers;
    uint64_t dds_register;
    
    // Iterate through the frequencies, generate 1000 samples
    int samples = 10000;
    double endFreq   = 2000000000.;
    double startFreq = 1000000000.;
    double increment = (endFreq - startFreq) / samples;
    for(int i = 0; i < 10000; i++) {
        
        // Increment the requested frequency
        double requestedFreq = startFreq + increment * i;
        HTsetFrequency(tuner, requestedFreq);
        
        // Get the registers
        LMXgenerateRegisters(tuner->pll, &registers);
        dds_register = DDSgenerateRegisters(tuner->dds);
        
        // Create the transitions
        command_t *transitions = newCommand();
        appendTransferSPI(transitions, 0, 1, 2, 21, registers.nDividerRegister);
        appendTransferSPI(transitions, 0, 1, 2, 21, registers.rDividerRegister);
        insertTransferSPI(transitions, 3, 4, 5, 40, dds_register, 0);
        
        // Send the transitions to the hardware
        results = driver->command(driver, transitions->length, transitions->array);
        
        // Clean up
        free(results);
        freeCommand(transitions);
        
        // Print a hash marker for the progress bar
        if (i % 125 == 0) {
            printf("#");
            fflush(stdout);
        }
    }
    
    printf("\n");
    
    return 0;
}
