//
//  spi_test.c
//  MSA
//
//  Created by William Dillon on 4/10/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <strings.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include "spi.h"

int
main(void)
{
    int retval;
    
    retval = initializeSPI(20000000,       // Default to 20MHz
                           SPI_MODE_0,     // SPI mode 0
                           0,              // Default delay between transfers
                           8,              // 8 bit words
                           0);             // SPI device 0

    if(retval) {
        fprintf(stderr, "Unable to initialize SPI.\n");
        return -1;
    }
    
    // Print a progress bar (assume 80 columns)
    printf("----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----|\n");

    // Create a set of 8 x 16 bit messages to cycle through
//    uint16_t txBuffer[8] = {0xDEAD, 0xBEEF, 0xBAAD, 0xF00D,
//                            0xDEAD, 0xBEEF, 0xBAAD, 0xF00D};

    // In order to accurately test the time required to affect a tuning
    // We'll take Scotty at his word and assume that it'll take 126 sets of 16 bits
    // So, let's create an array this size, fill it with random data, and let it rip.
    uint16_t txBuffer[126];
    for(int i = 0; i < 63; i++) {
        txBuffer[i * 2 + 0] = 0xAAAA;
        txBuffer[i * 2 + 1] = 0x5555;
    }
    
    // Do the inner test ten thousand times (10000 * 126 * 2 = 2.52 MB)
    for(int i = 0; i < 10000; i++) {
//        if(putSPIData(0, sizeof(txBuffer), txBuffer)) {
//            fprintf(stderr, "Error during SPI transfer!\n");
//            return 1;
//        }

        if (putSPIData16(0, 126, txBuffer)) {
            fprintf(stderr, "Error during SPI transfer!\n");
            return 1;
        }
        
        // Print a hash marker for the progress bar (assume 80 columns)
        if (i % 125 == 0) {
            printf("#");
            fflush(stdout);
        }
    }

    printf("\n");
    
    return 0;
}

