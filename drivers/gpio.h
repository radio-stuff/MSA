//
//  gpio.h
//  MSA
//
//  Created by William Dillon on 4/10/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#ifndef MSA_gpio_h
#define MSA_gpio_h

// Use these macros to ensure expected behavior
#define HIGH 1
#define LOW  0

#define IN   1
#define OUT  0

// These functions access and modify pin state
// the pin is defined as the GPIO pin number according to the pinout.
// For example, GPIO22 would be the number 22.
// The state is given as either '0' or '1', the macros HIGH and LOW may be used.
int getStateForPin(int pin);
void setStateForPin(int pin, int state);

// This function is able to change the direction of pin behavior.
// Use the macros for in and out above.
void setDirectionForPin(int pin, int direction);

// This function must be run before using any of the above functions.
int initializeGPIO(void);
void GPIOclose(void);

#endif
