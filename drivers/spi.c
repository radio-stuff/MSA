//
//  spi.c
//  MSA
//
//  Created by William Dillon on 4/10/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

// This code is derived in part from the SPI Device documentation
// available here:
// http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/plain/Documentation/spi/spidev_test.c
// By Anton Vorontsov and MontaVista software <avorontsov@ru.mvista.com>
// Released under GPL V.2 

#include <strings.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

static int      deviceZeroFD    = -1;
static uint8_t  deviceZeroMode  = 0;
static uint8_t  deviceZeroBits  = 8;
static uint16_t deviceZeroDelay = 0;
static uint32_t deviceZeroSpeed = 500000;

static int      deviceOneFD    = -1;
static uint8_t  deviceOneMode  = 0;
static uint8_t  deviceOneBits  = 8;
static uint16_t deviceOneDelay = 0;
static uint32_t deviceOneSpeed = 500000;

// These functions transmit and receive SPI data in 8-bit chunks
int  getSPIData(int device, int bytes, char *rxBuff)
{
    int fd = (device)? deviceOneFD : deviceZeroFD;
    uint8_t  bits  = (device)? deviceOneBits : deviceZeroBits;
    uint16_t delay = (device)? deviceOneDelay : deviceZeroDelay;
    uint32_t speed = (device)? deviceOneSpeed : deviceZeroSpeed;
    
    // Create a temporary buffer for the transmitted data
    uint8_t *txBuff = (uint8_t *)malloc(bytes);
    bzero(txBuff, bytes);
    
    struct spi_ioc_transfer tr = {
        .tx_buf = (unsigned long)txBuff,
        .rx_buf = (unsigned long)rxBuff,
        .len = bytes,
        .delay_usecs = delay,
        .speed_hz = speed,
        .bits_per_word = bits
    };
    
    int retval = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
    free(txBuff);
    
    if (retval < 1) {
        perror("Unable to receive SPI message");
        return -1;
    }
    
    return 0;
}

// The way that the SPI driver works for 16 bit transfers is by bundling them
// into a single ioc transfer.  The CS pin is held low for the entire transfer
// regardless of the number of bytes transfered.  To do a series of 16 bit
// transfers, we can create a transfer array and bundle these discrete transfers
// together for maximum performance.  This will be done for the provided number
// of words.
int putSPIData16(int device, int words, uint16_t *buffer)
{
    int fd = (device)? deviceOneFD : deviceZeroFD;

    for(int i = 0; i < words; i++) {
        write(fd, &buffer[i], 2);
    }

    return 0;

    /* This code may be faster someday, but for now it doesn't behave as expected.
    struct spi_ioc_transfer *msgs = (struct spi_ioc_transfer *)malloc(sizeof(struct spi_ioc_transfer) * words);
    if (msgs == NULL) {
        fprintf(stderr, "Unable to allocate space for SPI transfers array\n");
        return -1;
    }
    
    // Get a bit memory to be used for the 'reads' that occur during writes
    uint16_t readData;

    // Get the necessary fields
    uint8_t  bits  = (device)? deviceOneBits : deviceZeroBits;
    uint16_t delay = (device)? deviceOneDelay : deviceZeroDelay;
    uint32_t speed = (device)? deviceOneSpeed : deviceZeroSpeed;

    // Fill the msgs with our data
    for (int i = 0; i < words; i++) {
        msgs[i].tx_buf = (unsigned long)&buffer[i];
        msgs[i].rx_buf = (unsigned long)&readData;
        msgs[i].len    = 2; // bytes
        msgs[i].delay_usecs = delay;
        msgs[i].speed_hz = speed;
        msgs[i].bits_per_word = 8; // fixed
    }
    
    int retval = ioctl(fd, SPI_IOC_MESSAGE(words), msgs);
    
    if (retval < 1) {
        perror("Unable to send 16 bit SPI data\n");
        return -1;
    }
    
    return 0;
     */
}


int  putSPIData(int device, int count, char *txBuff)
{
    int fd = (device)? deviceOneFD : deviceZeroFD;
    
    // Each write call will toggle the CS pin.
    for(int i = 0; i < count; i++) {
        write(fd, &txBuff[i], 1);
    }
    
    return 0;
}

int initializeSPI(uint32_t speed,
                  uint8_t mode,
                  uint16_t delay,
                  uint8_t bits,
                  int device)
{
    char *deviceString;

    // Get the SPI device file name by device number
    switch (device) {
        case 0:
            deviceString = "/dev/spidev0.0";
            deviceZeroMode  = mode;
            deviceZeroDelay = delay;
            deviceZeroBits  = bits;
            deviceZeroSpeed = speed;
            break;
        case 1:
            deviceString = "/dev/spidev0.1";
            deviceOneMode  = mode;
            deviceOneDelay = delay;
            deviceOneBits  = bits;
            deviceOneSpeed = speed;
            break;
            
        default:
            fprintf(stderr, "SPI Device number out of bounds (%d).\n", device);
            return -1;
            break;
    }
    
    // Attempt to open the device
    int fd = open(deviceString, O_RDWR);
    if (fd < 0) {
        perror("Unable to open SPI device");
        return -1;
    }
    
    // Attempt to set the device properties
    int retval = ioctl(fd, SPI_IOC_WR_MODE, &mode);
    if (retval == -1) {
        perror("Unable to set SPI operation mode for writing");
        close(fd);
        return -1;
    }

    retval = ioctl(fd, SPI_IOC_RD_MODE, &mode);
    if (retval == -1) {
        perror("Unable to set SPI operation mode for reading");
        close(fd);
        return -1;
    }

    // Attempt to set SPI bits per word
    retval = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if (retval == -1) {
        perror("Unable to set SPI bits per word for writing");
        close(fd);
        return -1;
    }

    retval = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
    if (retval == -1) {
        perror("Unable to set SPI bits per word for reading");
        close(fd);
        return -1;
    }

    // Attempt to set SPI speed
    retval = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (retval == -1) {
        perror("Unable to set SPI speed for writing");
        close(fd);
        return -1;
    }

    retval = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
    if (retval == -1) {
        perror("Unable to set SPI speed for reading");
        close(fd);
        return -1;
    }

    // Save the file descriptor and
    // Save the achieved values back into the state variables
    if (!device) {
        deviceZeroFD = fd;
        deviceZeroMode  = mode;
        deviceZeroBits  = bits;
        deviceZeroSpeed = speed;
    } else {
        deviceOneFD  = fd;
        deviceOneMode  = mode;
        deviceOneBits  = bits;
        deviceOneSpeed = speed;
    }
    
    return 0;
}

void SPIclose(void)
{
    return;
}
