//
//  serial.c
//  MSA
//
//  Created by William Dillon on 4/30/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include "serial.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>

#define SERIAL_PORT_PARITY_MASK		0x38
#define SERIAL_PORT_DATABIT_MASK	0x06
#define SERIAL_PORT_STOPBIT_MASK	0x01

// Add other OSs here when these functions are defined for them.
#if defined __APPLE__ && __MACH__
#include <IOKit/IOKitLib.h>
#include <IOKit/serial/IOSerialKeys.h>
#if defined(MAC_OS_X_VERSION_10_3) && (MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_3)
#include <IOKit/serial/ioss.h>
#endif
#include <IOKit/IOBSD.h>

static int portCount = -1;
static char *portListNames[128]; // Maximum of 128 serial ports
static char *portListPaths[128]; // Same as names

int  serialCountPorts()
{
    if (portCount >= 0) {
        return portCount;
    }
    
    kern_return_t           kernResult;
    CFMutableDictionaryRef  classesToMatch;
	io_iterator_t			matchingServices;

    // Serial devices are instances of class IOSerialBSDClient
    classesToMatch = IOServiceMatching(kIOSerialBSDServiceValue);
    if (classesToMatch == NULL) {
        fprintf(stderr, "IOServiceMatching returned a NULL dictionary.\n");
        return -1;
    } else {
        CFDictionarySetValue(classesToMatch,
                             CFSTR(kIOSerialBSDTypeKey),
                             CFSTR(kIOSerialBSDRS232Type));
    }
    
    kernResult = IOServiceGetMatchingServices(kIOMasterPortDefault,
                                              classesToMatch,
                                              &matchingServices);
    if (KERN_SUCCESS != kernResult)
    {
        printf("IOServiceGetMatchingServices returned %d\n", kernResult);
        return -1;
    }

    portCount = 0;
	io_object_t	modemService;
    while ((modemService = IOIteratorNext(matchingServices)))
    {
        CFTypeRef pathCFStr;
		CFTypeRef nameCFStr;
		
        // Get the dialin device's path (/dev/tty.xxxxx).
		pathCFStr = IORegistryEntryCreateCFProperty(modemService,
                                                    CFSTR(kIODialinDeviceKey),
                                                    kCFAllocatorDefault,
                                                    0);
        
        // Get the device name
		nameCFStr = IORegistryEntryCreateCFProperty(modemService,
                                                    CFSTR(kIOTTYDeviceKey),
                                                    kCFAllocatorDefault,
                                                    0);
		
		if (pathCFStr && nameCFStr)
        {
            // Increment the port counter and add the port only if it'll fit
            if (portCount < 128) {
                // Copy the port name from the CF string
                CFIndex nameLength = CFStringGetLength(nameCFStr) + 1;
                portListNames[portCount] = (char *)malloc(nameLength);
                if (portListNames[portCount]) {
                    CFStringGetCString(nameCFStr,
                                       portListNames[portCount],
                                       nameLength,
                                       kCFStringEncodingUTF8);
                }

                // Copy the port path from the CF string
                CFIndex pathLength = CFStringGetLength(pathCFStr) + 1;
                portListPaths[portCount] = (char *)malloc(pathLength + 1);
                if (portListPaths[portCount]) {
                    CFStringGetCString(pathCFStr,
                                       portListPaths[portCount],
                                       pathLength,
                                       kCFStringEncodingUTF8);
                }
                
                portCount++;
            } else {
                fprintf(stderr, "Warning: skipping serial port because there are more than 128 of them in this system.\n");
            }
        }

        if (pathCFStr) CFRelease(pathCFStr);
        if (nameCFStr) CFRelease(nameCFStr);
        
		IOObjectRelease(modemService);
    }

	return portCount;
}

char **serialListPortNames()
{
    if (portCount < 0) {
        serialCountPorts();
    }
    return portListNames;
}

char **serialListPortPaths()
{
    if (portCount < 0) {
        serialCountPorts();
    }
    return portListPaths;
}
#endif

// Creator and destructor
serial_port_t *newSerialPort(char *fileName, int speed, int flags)
{
    serial_port_t *driver = (serial_port_t *)malloc(sizeof(serial_port_t));
    if (driver == NULL) {
        return NULL;
    }
    
   	struct termios tty;
	fprintf(stderr, "Attempting to open tty: %s\n", fileName);
	
	// Must open the TTY "non-blocking"
	driver->serialFD = open(fileName, O_RDWR | O_NOCTTY | O_NONBLOCK );
	if( driver->serialFD < 0 ) {
		perror("Opening file");
		goto error;
	}
	
	if( ioctl(driver->serialFD, TIOCEXCL) == -1 ) {
		perror("Setting exclusive access to serial port");
		goto error;
	}
	
	if( fcntl(driver->serialFD, F_SETFL, 0) != 0 ) {
		perror("Enable blocking I/O");
		goto error;
	}
	
	fprintf(stderr, "Opened tty device file descriptor: %d.\n", driver->serialFD);
	
	if( tcgetattr(driver->serialFD, &(driver->orig)) != 0 ) {
		perror("Getting serial settings");
		goto error;
	}
	
	// Copy original settings to copy defaults
	tty = driver->orig;
	
	tty.c_iflag = IGNBRK;	// Ignore breaks and parity errors
	tty.c_oflag = 0;		// No special output needs
	tty.c_lflag = 0;		// No local setting needs
	tty.c_cflag = CREAD;	// Enable receiver
	
    // Set the raw input (non-canonical) mode, where reads block until
    // at least one character is received, or a second elapses.
	cfmakeraw(&tty);
	tty.c_cc[VMIN]	= 1;
	tty.c_cc[VTIME]	= 10;
	
	// Set the number of databits from the flags
	switch(flags & SERIAL_PORT_DATABIT_MASK) {
		case SERIAL_PORT_5BIT:
			tty.c_cflag |= CS5;
			break;
		case SERIAL_PORT_6BIT:
			tty.c_cflag |= CS6;
			break;
		case SERIAL_PORT_7BIT:
			tty.c_cflag |= CS7;
			break;
		case SERIAL_PORT_8BIT:
			tty.c_cflag |= CS8;
	}
	
	// Set the parity from flag bits (mark and space not yet implemented)
	switch(flags & SERIAL_PORT_PARITY_MASK) {
		case SERIAL_PORT_EVEN:
			tty.c_cflag |= PARENB;
			break;
		case SERIAL_PORT_ODD:
			tty.c_cflag |= PARENB | PARODD;
			break;
		case SERIAL_PORT_NONE:
			tty.c_iflag |= IGNPAR;
			break;
		default:
			fprintf(stderr, "Parity setting not yet implemented or invalid.\n");
	}
	
	if( !(flags & SERIAL_PORT_HWFLC) ) {
		tty.c_cflag |= CLOCAL;	// Disable HW flow control
	} else {
		tty.c_cflag |= CCTS_OFLOW | CRTS_IFLOW;
        printf("Set hardware flow control\n");
	}
	
	if( flags & SERIAL_PORT_STOPBIT_MASK ) {
		tty.c_cflag |= CSTOPB;	// Send 2 stop bits
	}
    
	fflush(stdout);
    
    /*
	if( cfsetspeed(&tty, speed) != 0 ) {
		fprintf(stderr, "Setting speed to %d baud: %s", speed, strerror(errno));
		goto error;
	}
	
    printf("Set baud rate to %d\n", (int) cfgetispeed(&tty));
	fflush(stdout);
    */
    
	if( tcsetattr(driver->serialFD, TCSANOW, &tty) != 0 ) {
		perror("Setting serial port attributes");
		goto error;
	}
    
    printf("Set flags.\n");
	fflush(stdout);

    if ( ioctl(driver->serialFD, IOSSIOSPEED, &speed) == -1 )
    {
        printf( "Error %d calling ioctl( ..., IOSSIOSPEED, ... )\n", errno );
    } else {
        printf("Set speed to %d\n", speed);
    }
    
	// Get the 'BEFORE' line bits
	ioctl(driver->serialFD, TIOCMGET, &(driver->flags));
	fprintf(stderr, "Flags are 0x%x.\n", (driver->flags));
	fflush(stdout);
	
	return driver;
	
error:
	free(driver);
	return NULL;
}

void closeSerialDriver(serial_port_t *driver)
{
    // Drain the buffers
	if (tcdrain(driver->serialFD) == -1) {
		perror("Waiting for drain");
	}
	
    // Return the port to previous settings
	if (tcsetattr(driver->serialFD, TCSANOW, &(driver->orig)) == -1) {
		perror("Restoring original port settings");
	}
	
    // Close the serial port descriptor
	close(driver->serialFD);
	
    free(driver);
}

void serialSendBreak(serial_port_t *driver)
{
    tcsendbreak(driver->serialFD, 0);
}

void serialSetRTS(serial_port_t *driver, uint8_t state)
{
	int temp = driver->flags;
    // Set or clear RTS according to the desired state
	
	if(state) {
		temp	|=  TIOCM_RTS;
	} else {
		temp	&= ~TIOCM_RTS;
	}
	
	ioctl(driver->serialFD, TIOCMSET, &temp);
	fprintf(stderr, "Setting %x.\n", temp);
}

void serialResetRTS(serial_port_t *driver)
{
    // Reset to the 'BEFORE' state
	ioctl(driver->serialFD, TIOCMSET, &(driver->flags));
	fprintf(stderr, "Setting %x.\n", driver->flags);
}

uint8_t serialGetByte(serial_port_t *driver)
{
	ssize_t retval;
	char temp;
	
	while( (retval = read(driver->serialFD, &temp, 1)) == 0 ) {
		fprintf(stderr, "Zero-length read.\n");
	}
	
	if( retval < 0 ) {
		perror("Filling buffer");
		driver->error = 1;
	}
    
	driver->error = 0;
	return temp;
}

// In the following functions, return is boolean for success (0 = success)
uint8_t serialPutByte(serial_port_t *driver, uint8_t byte)
{
    if( write(driver->serialFD, &byte, 1) != 1 ) {
		perror("Writing byte to serial port");
		return 1;
	}
	
	return 0;
}

uint8_t serialGetData(serial_port_t *driver, uint32_t length, uint8_t *buffer)
{
	size_t retval, bytes = 0;
		
	// Fill the Buffer:
	while( bytes < length ) {
		while( (retval = read(driver->serialFD, &buffer[bytes], length-bytes)) == 0 );
		
		if( retval > 0 ) {
//            for (int i = 0; i < retval; i++) {
//                printf("Received byte %ld 0x%02x\n", bytes + i, buffer[bytes + i]);
//            }
			bytes += retval;
		} else {
			perror("Filling buffer");
		}
	}
		
	return 0;
}

uint8_t serialPutData(  serial_port_t *driver, uint32_t length, uint8_t *buffer)
{
	size_t retval, bytes = 0;
	
//    printf("Writing data (%d bytes)\n", length);
    
	// Fill the Buffer:
	while( bytes < length ) {
		while( (retval = write(driver->serialFD, buffer, length-bytes)) == 0 );
		
		if( retval > 0 ) {
			bytes += retval;
		} else {
			perror("Writing data");
			return 1;
		}
	}
	
	return 0;
}

uint8_t serialPutString(serial_port_t *driver, uint8_t *string)
{
    int length = StrLength(string);
    return serialPutData(driver, length, string);
}
