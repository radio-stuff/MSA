#include <stdio.h>
#include <unistd.h>
#include "gpio.h"

int
main(void)
{
    int retval;

    retval = initializeGPIO();
    if(retval) {
        fprintf(stderr, "Unable to initialize GPIO.\n");
        return -1;
    }

    // Set GPIO4 to output
    setDirectionForPin(4, OUT);

    // Print a progress bar (assume 80 columns)
    printf("----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----|\n");
    
    // Toggle it for a minute, once a second
    for(int i = 0; i < 1000000; i++) {
        for(int j = 0; j < 1000; j++) {
            setStateForPin(4, HIGH);
            setStateForPin(4, LOW);
        }
        
        // Print a hash marker for the progress bar
        if (i % 12500 == 0) {
            printf("#");
            fflush(stdout);
        }
    }

    printf("\n");

    return 0;
}
