//
//  raspi_driver.c
//  MSA
//
//  Created by William Dillon on 4/11/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "driver.h"
#include "spi.h"
#include "gpio.h"

typedef struct {
    // This variable contains any other bits (above the bottom 12) that are held high
    uint16_t otherBits;
    
} raspi_driver_t;

void setOtherBits(raspi_driver_t *driver, uint16_t bits)
{
    // Mask out the lower 12 bits to ensure that it doesn't conflict with the
    // operation of the latches
    driver->otherBits = bits & 0xF000;
}

int raspiInit(void *driverStruct, char *unused)
{
    raspi_driver_t *driver = ((driver_t *)driverStruct)->driverContext;

    // Initialize GPIO
    if(initializeGPIO()) {
        fprintf(stderr, "Unable to initialize the GPIO interface.\n");
        return 1;
    }

    // Initialize SPI
    if (initializeSPI(2000000, SPI_MODE_0, 0, 8, 0)) {
        fprintf(stderr, "Unable to initialize the SPI interface.\n");
        return 2;
    }
    
    return 0;
}

// Write out to the ports the count is the number of transitions
// and the guffer must contain 4x as many bytes as transitions
int raspiWrite(void *driverStruct, uint16_t count, uint32_t *buffer)
{
    raspi_driver_t *driver = ((driver_t *)driverStruct)->driverContext;
    // The theory of operation for this function is to load the transitions into
    // the control board using the SPI bus.  The latch pins are connected through
    // the SPI shift registers as well, so each load takes two SPI transfers.
    // One transfer loads the new data and the latch bit 'high',
    // the next transfer keeps the data the same, and brings the latch 'low'.
    // This process is repeated for each of the 4 latches.
    
    // Create an array of uint16_t values, one per transition.
    uint16_t *transitions = (uint16_t *)malloc(sizeof(uint16_t) * count * 8);
    
    // Iterate per transition
    for (uint16_t i = 0; i < count; i++) {
        uint8_t latchByte = buffer[i] & 0x000000FF;
        transitions[(i * 8) + 0] = latchByte | driver->otherBits | (1 << (8 + 0));
        transitions[(i * 8) + 1] = latchByte | driver->otherBits;
        latchByte = (buffer[i] >>  8) & 0x000000FF;
        transitions[(i * 8) + 2] = latchByte | driver->otherBits | (1 << (8 + 1));
        transitions[(i * 8) + 3] = latchByte | driver->otherBits;
        latchByte = (buffer[i] >> 16) & 0x000000FF;
        transitions[(i * 8) + 4] = latchByte | driver->otherBits | (1 << (8 + 2));
        transitions[(i * 8) + 5] = latchByte | driver->otherBits;
        latchByte = (buffer[i] >> 24) & 0x000000FF;
        transitions[(i * 8) + 6] = latchByte | driver->otherBits | (1 << (8 + 3));
        transitions[(i * 8) + 7] = latchByte | driver->otherBits;
    }
    
    // Send the transition list over to the SPI peripheral
    if(putSPIData16(0, count * 4, transitions)) {
        fprintf(stderr, "Error sending data using SPI!\n");
        return -1;
    }
    
    return 0;
}

// Read from status lines, (right justified 4 bits) for a given number of
// periods of time.
int raspiRead(void *driverStruct, uint16_t periods, uint8_t *buffer)
{
    raspi_driver_t *driver = ((driver_t *)driverStruct)->driverContext;

    return -1;    
}

// Cleanup
int raspiClose(void *driverStruct)
{
    raspi_driver_t *driver = ((driver_t *)driverStruct)->driverContext;

    GPIOclose();
    SPIclose();

    return 0;
}

driver_t *newRaspiDriver(int SPIrate)
{
    driver_t *driver = (driver_t *)malloc(sizeof(driver_t));
    if (driver == NULL) {
        fprintf(stderr, "Unable to create driver struct\n");
        return NULL;
    }
    
    raspi_driver_t *raspi_driver = (raspi_driver_t *)malloc(sizeof(raspi_driver_t));
    if (raspi_driver == NULL) {
        fprintf(stderr, "Unable to create memory for RasPi Driver context.\n");
        return NULL;
    }
    
    // Link the function pointers into the driver structure
    driver->driverContext = raspi_driver;
//    driver->read  = raspiRead;
//    driver->write = raspiWrite;
//    driver->init  = raspiInit;
    driver->close = raspiClose;
    
    return driver;
}
