//
//  cypress_generic.h
//  MSA
//
//  Created by William Dillon on 4/10/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//
// This file contains the OS-neutral functions for initializing and
// accessing the USB device for the spectrum analyzer.  It's important
// that each of these functions behaves exactly the same way in win,
// mac, and linux.  I know this is possible using libusb on linux and
// IOKit on mac.  It may be necessary to use libusb in windows.

#ifndef MSA_usb_generic_h
#define MSA_usb_generic_h

// This function is responsible for finding the cypress USB device
// and loading its firmware over USB.  Once the firmware is loaded,
// the device will perform a bus reset and re-enumerate.  Many OSs
// will require that the enumeration process occur asynchronously.
// In this case, that must happen in a seperate thread, and as lock
// in this function should wait for that to finish.  It is assumed
// that when this function returns the endpoints are open and ready.
int findDevice();

int sendRegisters()


typdef usbDeviceType struct {
    int find*(void);
    int send*(void *, int size, char *buffer);
} usbDevice;


#endif
