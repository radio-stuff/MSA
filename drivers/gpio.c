// gpio.c
//
// (C) William Dillon 2013
// Relesed under the GPL V.2 license
//
// Some parts of this code are derrived from the Dom and Gert example code.


#define BCM2708_PERI_BASE        0x20000000
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include "gpio.h"

#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

int  mem_fd;
void *gpio_map;

// I/O access
static volatile unsigned *gpio;

// GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x) or SET_GPIO_ALT(x,y)
#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) *(gpio+(((g)/10))) |= (((a)<=3?(a)+4:(a)==4?3:2)<<(((g)%10)*3))

#define GPIO_SET *(gpio+7)  // set    bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10) // clears bits which are 1 ignores bits which are 0


int getStateForPin(int pin)
{
    return -1;
}

void setStateForPin(int pin, int state)
{
    int mask = 1 << pin;

    if(gpio == NULL) {
        fprintf(stderr, "Must initialize before use of GPIO.\n");
        return;
    }

    if (state) {
        GPIO_SET = mask;
    } else {
        GPIO_CLR = mask;
    }

    return;
}

void setDirectionForPin(int pin, int direction)
{
    if(gpio == NULL) {
        fprintf(stderr, "Must initialize before use of GPIO.\n");
        return;
    }

    // Before setting to 'out' it must be set 'in'

    INP_GPIO(pin);
    if(!direction) {
        OUT_GPIO(pin);
    }
}

int initializeGPIO(void)
{
   /* open /dev/mem */
   if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
      printf("can't open /dev/mem \n");
      return -1;
   }

   /* mmap GPIO */
   gpio_map = mmap(
      NULL,             //Any adddress in our space will do
      BLOCK_SIZE,       //Map length
      PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
      MAP_SHARED,       //Shared with other processes
      mem_fd,           //File to map
      GPIO_BASE         //Offset to GPIO peripheral
   );

   close(mem_fd); //No need to keep mem_fd open after mmap

   if (gpio_map == MAP_FAILED) {
      printf("mmap error %d\n", (int)gpio_map);//errno also set!
      return -1;
   }

   // Always use volatile pointer!
   gpio = (volatile unsigned *)gpio_map;

   return 0;
}

void GPIOclose(void)
{
    // Unmap the GPIO memory
    munmap(gpio_map, BLOCK_SIZE);
}
