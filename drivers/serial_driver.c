//
//  serial_driver.c
//  MSA
//
//  Created by William Dillon on 4/30/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "driver.h"

uint8_t *serialCommand(void *driverVoid, uint16_t transactions, uint32_t *buffer)
{
    driver_t *driver = (driver_t *)driverVoid;
    serial_port_t *port = (serial_port_t *)driver->driverContext;
    
    uint8_t *returnBuffer = (uint8_t *)malloc(transactions);
    if (returnBuffer == NULL) {
        return NULL;
    }
    
    // Send the transaction data to the serial port
    serialPutData(port, transactions * 4, (uint8_t *)buffer);
    serialGetData(port, transactions, returnBuffer);
    
    return returnBuffer;
}

int serialClose(void *driverVoid)
{
    driver_t *driver = (driver_t *)driverVoid;
    serial_port_t *port = (serial_port_t *)driver->driverContext;

    closeSerialDriver(port);
    driver->driverContext = NULL;
    return 0;
}

driver_t *newSerialDriver(int baudRate, char *path)
{
    // Create an instance of the driver type
    driver_t *driver = (driver_t *)malloc(sizeof(driver_t));
    if (driver == NULL) {
        return NULL;
    }

    serial_port_t *port = newSerialPort(path, baudRate, SERIAL_PORT_N81);

    // The process of opening the port resets the chipkit
    // we have to wait for the bootloader to finish.
    sleep(5);

    // Fill the driver structure with data
    driver->driverContext = port;
    driver->command = serialCommand;
    driver->close = serialClose;
    
    return driver;
}