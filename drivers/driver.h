//
//  driver.h
//  MSA
//
//  Created by William Dillon on 4/11/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#ifndef MSA_driver_h
#define MSA_driver_h

#include <stdint.h>
#include "serial.h"

// This struct stores all the information needed for the drivers as a void *
// and stores the set of function pointers necessary for operation of the
// driver
typedef struct {
    
    // Pointer to the drviers context structure.
    // This should be the first argument to every driver function
    void *driverContext;
    
// Function pointers
    // Initialization, the character array may be used for device specific options
//    int (*init)(void *);
/*
	// Write out to the ports.  The count is the number of transitions
    // and the buffer must contain 4x as many bytes as transitions (4 latches)
    int (*write)(void *, uint16_t transitions, uint32_t *buffer);
    
    // Read from status lines, (right justified 4 bits) for a given number
    // periods of time.
	int (*read)(void *, uint16_t periods, uint8_t *buffer);
*/
    
    // Command the MSA.  The pin transitions are inserted into the buffer
    // and the status bit states are returned.  The returned status bits
    // must be freed after use.
    uint8_t *(*command)(void *, uint16_t transactions, uint32_t *buffer);
    
    // Cleanup
	int (*close)(void *);
    
} driver_t;

driver_t *newRaspiDriver(int SPIrate);
driver_t *newSerialDriver(int baudRate, char *path);

#endif
