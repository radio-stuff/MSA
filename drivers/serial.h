//
//  serial.h
//  MSA
//
//  Created by William Dillon on 4/30/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#ifndef MSA_serial_h
#define MSA_serial_h

#include <stdint.h>
#include <termios.h>

//	Common defaults
#define SERIAL_PORT_N81	0x00	// Default (no flow control)
#define SERIAL_PORT_E71	0x0C	// Default (no flow control)

//	All options (bit-wise 'or'd)
#define SERIAL_PORT_NONE  0x00
#define SERIAL_PORT_EVEN  0x08
#define SERIAL_PORT_ODD   0x10
#define SERIAL_PORT_MARK  0x18
#define SERIAL_PORT_SPACE 0x20

#define SERIAL_PORT_8BIT  0x00
#define SERIAL_PORT_5BIT  0x02
#define SERIAL_PORT_6BIT  0x04
#define SERIAL_PORT_7BIT  0x06

#define SERIAL_PORT_1STOP 0x00
#define SERIAL_PORT_2STOP 0x01

#define SERIAL_PORT_HWFLC 0x40

typedef struct
{
    struct termios orig;
	int			   flags;
	int			   serialFD;
    int            error;
} serial_port_t;

// Add other OSs here when these functions are defined for them.
#if defined __APPLE__ && __MACH__
// Functions to discover available serial ports
// Call count ports first, then expect an array for that many pointers.
// The names array contains service names when available.
int  serialCountPorts();
char **serialListPortNames();
char **serialListPortPaths();
#endif

// Creator and destructor
serial_port_t *newSerialPort(char *fileName, int portSpeed, int flags);
void closeSerialDriver(serial_port_t *driver);

void serialSetRTS(   serial_port_t *driver, uint8_t state);
void serialResetRTS( serial_port_t *driver);
void serialSendBreak(serial_port_t *driver);

uint8_t serialGetByte(serial_port_t *driver);

// In the following functions, return is boolean for success (0 = success)
uint8_t serialPutByte(  serial_port_t *driver, uint8_t byte);
uint8_t serialGetData(  serial_port_t *driver, uint32_t length, uint8_t *buffer);
uint8_t serialPutData(  serial_port_t *driver, uint32_t length, uint8_t *buffer);
uint8_t serialPutString(serial_port_t *driver, uint8_t *string);

#endif
