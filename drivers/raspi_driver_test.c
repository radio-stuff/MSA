//
//  raspi_driver_test.c
//  MSA
//
//  Created by William Dillon on 4/12/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <stdio.h>
#include <unistd.h>
#include "raspi_driver.h"
#include "../analyzer/commander.h"
#include "../analyzer/hybrid_tuner.h"

int
main(void)
{
    // Create an instance of the raspi driver (20 MHz)
    driver_t *driver = newRaspiDriver(20000000);
    
    if (driver == NULL) {
        fprintf(stderr, "Unable to create the RasPi driver.\n");
        return 1;
    }
    
    // Test loading a set of known values into the control board
    uint32_t value = 0xDEADBEEF;
    driver->write(driver, 1, &value);
    
    // Test loading an SPI transmission
    command_t *command = newCommand();
    initPins(0x00000000);
    appendTransferSPI(command, 12, 16, 20, 16, 0xAA55);
    insertTransferSPI(command,  0,  4,  8, 16, 0x55AA, 0);
    driver->write(driver, command->length, command->array);
    
    // Test reading from the ADC
    uint8_t adcValues[16];
    driver->read(driver, 16, adcValues);
    
    // Print a progress bar (assume 80 columns)
    printf("----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----|\n");
    
    // Generate 10000 fake tunings to banchmark scan rate
    hybrid_tuner_t *tuner = newHybridTuner(4118, 1);
    if (tuner == NULL) {
        fprintf(stderr, "Unable to initialize the tuner");
        return 1;
    }
    
    HTsetRefClock(tuner, 64000000);
    HTsetIdealDDSFreq(tuner, 10700000);
    HTsetIdealPhaseFreq(tuner, 972000);
    
    LMX_registers_t registers;
    uint64_t dds_register;
    
    // Iterate through the frequencies, generate 1000 samples
    int samples = 10000;
    double endFreq   = 2000000000.;
    double startFreq = 1000000000.;
    double increment = (endFreq - startFreq) / samples;
    for(int i = 0; i < 10000; i++) {
        
        // Increment the requested frequency
        double requestedFreq = startFreq + increment * i;
        HTsetFrequency(tuner, requestedFreq);
        
        // Get the registers
        LMXgenerateRegisters(tuner->pll, &registers);
        dds_register = DDSgenerateRegisters(tuner->dds);
        
        // Create the transitions
        command_t *transitions = newCommand();
        appendTransferSPI(transitions, 0, 1, 2, 21, registers.nDividerRegister);
        appendTransferSPI(transitions, 0, 1, 2, 21, registers.rDividerRegister);
        insertTransferSPI(transitions, 3, 4, 5, 40, dds_register, 0);
        
        // Send the transitions to the hardware
        driver->write(driver, command->length, command->array);
        
        // Clean up
        freeCommand(command);
        
        // Print a hash marker for the progress bar
        if (i % 12500 == 0) {
            printf("#");
            fflush(stdout);
        }
    }
    
    printf("\n");

    return 0;
}
