//
//  spi.h
//  MSA
//
//  Created by William Dillon on 4/10/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#ifndef MSA_spi_h
#define MSA_spi_h

#include <linux/spi/spidev.h>

// This function will send a series of 16 bit transfers over the SPI bus
int putSPIData16(int device, int words, uint16_t *buffer);

// These functions transmit and receive SPI data in 8-bit chunks
// NOTE! They're only going to work with 8 bit transfers at this point!
int  getSPIData(int device, int bytes, char *buffer);
int  putSPIData(int device, int bytes, char *buffer);

// This function must be run before using any of the above functions.

// The speed is expressed in SPI clock rate in Hz.

// The mode is a bitwise OR of the following flags (defined in spidev.h):
// SPI_LOOP       - Loopback mode
// SPI_CPHA       - Clock Phase
// SPI_CPOL       - Clock Polarity
// SPI_LSB_FIRST  - Per-word bits-on-wire
// SPI_CS_HIGH    - Chip select active high
// SPI_3WIRE      - MOSI/MISO signals shared
// SPI_NO_CS      - Don't use chip select/enable
// SPI_READY      - Slave pulls low to pause

// Or, use the following standards:
// SPI_MODE_0              (0|0)
// SPI_MODE_1              (0|SPI_CPHA)
// SPI_MODE_2              (SPI_CPOL|0)
// SPI_MODE_3              (SPI_CPOL|SPI_CPHA)

// The delay option is the number of uSecs to wait after the last bit
// before deselecting the device before the next transfer

// The bits option is setting number of bits per transfer (CS toggle?)

// The device is the SPI device number, such as 0 or 1 as in
// /dev/spidev0.x
int initializeSPI(uint32_t speed,
                  uint8_t mode,
                  uint16_t delay,
                  uint8_t bits,
                  int device);

void SPIclose(void);

#endif
