//
//  analyzer.h
//  MSA
//
//  Created by William Dillon on 4/17/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#ifndef MSA_analyzer_h
#define MSA_analyzer_h

#include "hybrid_tuner.h"
#include "driver.h"

typedef struct {
    hybrid_tuner_t *tuner;
    hybrid_tuner_t *tracker;
    LMX_module_t   *lo2pll;
    
    driver_t *driver;
    
} analyzer_t;

analyzer_t *newAnalyzer(driver_t *driver);

double analyzerSetTunerFreq(  analyzer_t *analyzer, double frequency);
double analyzerSetTrackerFreq(analyzer_t *analyzer, double frequency);
void   analyzerGetADCValues(  analyzer_t *analyzer, double *magnitude, double *phase);

#endif
