//
//  hybrid_tuner.h
//  MSA
//
//  Created by William Dillon on 4/11/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#ifndef MSA_hybrid_tuner_h
#define MSA_hybrid_tuner_h

#include "../modules/AD_DDS.h"
#include "../modules/LMX_PLL.h"

// Users my access these values at any time, but it's not advisable to change them
// Changing these values through the functions below will ensure that they represent
// the actual values according to the hardware specifications.
typedef struct
{
    // Frequencies
    double refClock;
    double outFreq;
    double idealDDSFreq;
    double idealPhaseFreq;
    
    // Component modules
    LMX_module_t *pll;
    DDS_module_t *dds;
    
} hybrid_tuner_t;

// These functions modify the state of the dds struct.  They will return the
// closest value to what the user requested.  They will also store this value
// within the struct.  To update the hardware, call generateRegisters().
double HTsetFrequency(     hybrid_tuner_t *ht, double frequency);
void   HTsetRefClock(      hybrid_tuner_t *ht, double frequency);
void   HTsetIdealDDSFreq(  hybrid_tuner_t *ht, double frequency);
void   HTsetIdealPhaseFreq(hybrid_tuner_t *ht, double frequency);

// This function ensures that the module is setup correctly.
// This function will return an allocated dds struct.  This must be freed later.
// generateRegister MUST be run once and applied to the hardware for initialization.
hybrid_tuner_t * newHybridTuner(int pllType, int pllInvertingCP);

#endif
