//
//  commander.c
//  MSA
//
//  Created by William Dillon on 4/12/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "commander.h"

// These functions can only be used with a single command stream per application
// because of this static value
static uint32_t pinStates = 0;

void initPins(uint32_t states)
{
    pinStates = states;
}

// Create and initialize a new command
command_t *newCommand(void)
{
    command_t *newCommand = (command_t *)malloc(sizeof(command_t));
    if (newCommand == NULL) {
        fprintf(stderr, "Unable to allocate a new command.\n");
        return NULL;
    }
    
    newCommand->length  = 0;
    newCommand->array = NULL;
    newCommand->arrayLength = -1;
    
    return newCommand;
}

void freeCommand(command_t *command)
{
    free(command->array);
    free(command);
}

// This function ensures that the array is large enough to store the planned
// transitions.  If it isn't, it'll dynamically resize the array.  If the
// array hasn't been created yet, it will create one with the largest of the
// provided totalLength or DEFAULT_ARRAY_SIZE.
#define DEFAULT_ARRAY_SIZE 64
#define MAX(a,b) (((a)>(b))?(a):(b))
int checkArray(command_t *command, int totalLength)
{
    // Make sure that the array has been created in the first place.
    if (command->arrayLength < 0) {
        command->arrayLength = MAX(totalLength, DEFAULT_ARRAY_SIZE);
        command->array = (uint32_t *)malloc(sizeof(uint32_t) *
                                            command->arrayLength);
    }
    
    // Make sure that the array is big enough
    else if(totalLength > command->arrayLength) {
        command->arrayLength = totalLength;
        command->array = (uint32_t *)realloc(command->array,
                                             sizeof(uint32_t) *
                                             command->arrayLength);
    }
    
    if (command->array == NULL) {
        fprintf(stderr, "Unable to create or resize transition array.\n");
        return -1;
    }
    
    return 0;
}

void setPinState(command_t *command, int pinNumber, int state)
{
    // We're appending this state change to the command, so the required length
    // will always be one plus the current length, and the location is equal to
    // the current length.
    int location = command->length;
    if(checkArray(command, location+1)) {
        return;
    }   
    
    // Modify the current setting of the pins
    // Set the pin high
    if (state) {
        uint32_t change = (1 << pinNumber);
        pinStates |= change;
    }
    // Set the pin low (mask it out)
    else {
        uint32_t change = ~(1 << pinNumber);
        pinStates &= change;
    }
    
    // Put the new state into the command stream
    command->array[location] = pinStates;
    command->length = location + 1;
    return;
}

void appendTransferSPI(command_t *command,
                       int clockPin, int dataPin, int latchPin,
                       int numBits, uint64_t data)
{
    // It may make sense to use this as a front-end for the insert version
    insertTransferSPI(command,
                      clockPin, dataPin, latchPin,
                      numBits, data, command->length);
    return;
}

void insertTransferSPI(command_t *command,
                       int clockPin, int dataPin, int latchPin,
                       int numBits, uint64_t data, int loc)
{
    // Starting at the end of the current command list, add a new SPI transfer.
    // We require 3x the number of bits plus 2 to perform that transfer.
    int length   = numBits * 3 + 2;
    if (checkArray(command, loc + length)) {
        return;
    }
    
/*
 To calculate the number of cycles required we can use this formula:
 Bits * 3 + 2.  The reason for this is that we don't want anything changing
 during a clock change.  This is to ensure that there are no crosstalk effects.
 
         0                   1                   2   2
 offset  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2
        _: : : : : : : : : : : : : : : : : : : : : : :_
 LE     _\___________________________________________/
         : : : : : : : : : : : : : : : : : : : : : : :
        _     _     _     _     _     _     _     _
 Clock  _\___/ \___/ \___/ \___/ \___/ \___/ \___/ \___
         : : : : : : : : : : : : : : : : : : : : : : :
        ___ _____ _____ _____ _____ _____ _____ _______
 Data   ___X__6__X__5__X__4__X__3__X__2__X__1__X__0____ (bit inside)
         : : : : : : : : : : : : : : : : : : : : : : :
 
 */

    // Set the LE and clock pins on the SPI device low
    // The reason we're setting the pinstates also is because if we overrun the
    // existing data, we want to ensure that we fill it with pin states that
    // don't lift the latch pin.
    pinStates &= ~((1 << latchPin) | (1 << clockPin));
    command->array[loc++] = command->array[loc] &
                            ~((1 << latchPin) | (1 << clockPin));
    
    // Iterate through the bits
    // From now on, it's vital that we never alter the contents of the array
    // other than the pins that we've been assigned.
    uint64_t mask = (1 << (numBits - 1));
    for (int i = 0; i < numBits; i++) {
        // If we've already overrun the existing data,
        // fill array entries with the pinstates
        if (loc > command->length) {
            command->array[loc] = pinStates;
        }
        
        // First, change the data bit if its high
        if (data & mask) {
            command->array[loc+0] = command->array[loc+0] |  (1 << dataPin);
            command->array[loc+1] = command->array[loc+1] |  (1 << dataPin);
            command->array[loc+2] = command->array[loc+2] |  (1 << dataPin);
        }
        // If it's low
        else {
            command->array[loc+0] = command->array[loc+0] & ~(1 << dataPin);
            command->array[loc+1] = command->array[loc+1] & ~(1 << dataPin);
            command->array[loc+2] = command->array[loc+2] & ~(1 << dataPin);
        }
        loc++;
        
        // Then set the clock pin high
        if (loc > command->length) {
            command->array[loc] = pinStates;
        }
        command->array[loc++] = command->array[loc] | (1 << clockPin);
        
        // And low again
        if (loc > command->length) {
            command->array[loc] = pinStates;
        }
        command->array[loc++] = command->array[loc] & ~(1 << clockPin);
        
        // Shift the mask one bit to the right
        mask = mask >> 1;
    }
    
    // Set the LE pin on the SPI device high
    if (loc > command->length) {
        command->array[loc] = pinStates;
    }
    command->array[loc++] = command->array[loc] | (1 << latchPin);
    command->length = MAX(loc, command->length);
}

void insertConvertADC(command_t *command,
                      int clockPin, int convertPin,
                      int numBits, int loc)
{
    // We require 2x the number of bits plus 2 to perform that transfer.
    int length   = numBits * 2 + 2;
    if (checkArray(command, loc + length)) {
        return;
    }
    
/* This should be a quick reference for the protocol for working with the ADC
 the source for this graphic is available in the AD7685 datasheet, page 19,
 figure 37.  http://www.analog.com/static/imported-files/data_sheets/AD7685.pdf
 
 To calculate the number of cycles required we can use this formula:
 Bits * 2 + 2.  The example is 11 bits, so 11*2+2 = 24.
 
         0                   1                   2       2
 offset  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
         : : : : : : : : : : : : : : : : : : : : : : : : :
        ___                                               _
 Conv   _/ \_____________________________________________/
         : : : : : : : : : : : : : : : : : : : : : : : : :
        _____   _   _   _   _   _   _   _   _   _   _   ___
 Clock  _/   \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/
         : : : : : : : : : : : : : : : : : : : : : : : : :
               ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___
 SDO    -----_/10_X_9_X_8_X_7_X_6_X_5_X_4_X_3_X_2_X_1_X_0_>- (bit inside)
         : : : : : : : : : : : : : : : : : : : : : : : : :
 Sample   . . . * . * . * . * . * . * . * . * . * . * . * .
 
 The samples with '.' are ignored, but the samples with '*' are actual
 ADC samples, and are what we're interested in.
 */
    
    // First set the convert and clock pins high by default
    // (this settings are maintained between command invocations).
    pinStates |= (1 << convertPin) | (1 << clockPin);
    
    // Modify the current command array contents by changing the ADC bits
    // Offset 0
    command->array[loc++] = command->array[loc] |
                                 (1 << convertPin) | (1 << clockPin);
    
    // In the next command cycle, set the convert bit low
    // Offset 1
    command->array[loc++] = command->array[loc] & ~(1 << convertPin);
    
    // Iterate through the bits, it's vital that we never alter the
    // contents of the array other than the pins that we've been assigned.
    for (int i = 0; i < numBits; i++) {
        // If we've already overrun the existing data,
        // fill array entries with the pinstates
        if (loc > command->length) {
            command->array[loc] = pinStates;
        }
        
        // Set the clock pin low to enable the start
        // Even offsets 2,4,6,etc.
        command->array[loc++] = command->array[loc] | (1 << clockPin);
        
        if (loc > command->length) {
            command->array[loc] = pinStates;
        }

        // And low again
        // Odd offsets > 1:  3,5,7, etc.
        command->array[loc++] = command->array[loc] & ~(1 << clockPin);
    }
    
    // Set the convert pin high again
    if (loc > command->length) {
        command->array[loc] = pinStates;
    }
    command->array[loc++] = command->array[loc] | (1 << convertPin);
    command->length = MAX(loc, command->length);

}
