//
//  hybrid_tuner.c
//  MSA
//
//  Created by William Dillon on 4/11/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "hybrid_tuner.h"
#include "../modules/AD_DDS.h"
#include "../modules/LMX_PLL.h"

void HTrecalculate(hybrid_tuner_t *ht)
{
    // First determine the first pass value for the PLL and the ideal DDS tuning
    LMXsetRefClock(ht->pll, ht->idealDDSFreq);
    LMXsetPhaseFreq(ht->pll, ht->idealPhaseFreq);
    LMXsetFrequency(ht->pll, ht->outFreq);
    
    // Next, compute the frequency error of the PLL
    double error = ht->outFreq - ht->pll->outFreq;
    
    // Then, compute a new tuning for the DDS to correct the PLL error
    double correctedDDS = ht->idealDDSFreq + (error/ht->pll->int_n_divider) * ht->pll->int_r_divider;

    // Refresh the DDS and PLL computations
    DDSsetFrequency(ht->dds, correctedDDS);
    LMXsetRefClock( ht->pll, ht->dds->outFreq);
    LMXsetPhaseFreq(ht->pll, ht->idealPhaseFreq);
    LMXsetFrequency(ht->pll, ht->outFreq);

    // Update the tuners value to the achieved tuning
    ht->outFreq = ht->pll->outFreq;
}

double HTsetFrequency(hybrid_tuner_t *ht, double frequency)
{
    // Error checking
    if (ht == NULL) {
        fprintf(stderr, "Hybrid tuner function called with NULL structure.\n");
        return -1.;
    }

    if (ht->pll == NULL) {
        fprintf(stderr, "Hybrid tuner function called without a PLL.\n");
        return -1.;
    }

    if (ht->dds == NULL) {
        fprintf(stderr, "Hybrid tuner function called without a DDS.\n");
        return -1.;
    }
    
    ht->outFreq = frequency;
    HTrecalculate(ht);
    
    return ht->outFreq;
}

void HTsetRefClock(hybrid_tuner_t *ht, double frequency)
{
    // Error checking
    if (ht == NULL) {
        fprintf(stderr, "Hybrid tuner function called with NULL structure.\n");
        return;
    }
    
    if (ht->pll == NULL) {
        fprintf(stderr, "Hybrid tuner function called without a PLL.\n");
        return;
    }
    
    if (ht->dds == NULL) {
        fprintf(stderr, "Hybrid tuner function called without a DDS.\n");
        return;
    }
    
    DDSsetRefClock(ht->dds, frequency);
    HTrecalculate(ht);
    
    return;
}

void HTsetIdealDDSFreq(hybrid_tuner_t *ht, double frequency)
{
    // Error checking
    if (ht == NULL) {
        fprintf(stderr, "Hybrid tuner function called with NULL structure.\n");
        return;
    }
    
    if (ht->pll == NULL) {
        fprintf(stderr, "Hybrid tuner function called without a PLL.\n");
        return;
    }
    
    if (ht->dds == NULL) {
        fprintf(stderr, "Hybrid tuner function called without a DDS.\n");
        return;
    }
    
    ht->idealDDSFreq = frequency;
    HTrecalculate(ht);
    
    return;
}

void HTsetIdealPhaseFreq(hybrid_tuner_t *ht, double frequency)
{
    // Error checking
    if (ht == NULL) {
        fprintf(stderr, "Hybrid tuner function called with NULL structure.\n");
        return;
    }
    
    if (ht->pll == NULL) {
        fprintf(stderr, "Hybrid tuner function called without a PLL.\n");
        return;
    }
    
    if (ht->dds == NULL) {
        fprintf(stderr, "Hybrid tuner function called without a DDS.\n");
        return;
    }
    
    ht->idealPhaseFreq = frequency;
    HTrecalculate(ht);
    
    return;
}

hybrid_tuner_t * newHybridTuner(int pllType, int pllInvertingCP)
{
    // Allocate a structure
    hybrid_tuner_t *ht = (hybrid_tuner_t *)malloc(sizeof(hybrid_tuner_t));
    if (ht == NULL) {
        perror("Unable to allocate memory for DDS module structure");
        return NULL;
    }
    
    // Create its members
    ht->pll = newLMXPLL(pllType, pllInvertingCP);
    ht->dds = newDDS();
    
    if (ht->pll == NULL) {
        fprintf(stderr, "Unable to create PLL for the hybrid tuner");
        free(ht);
        return NULL;
    }
    
    if (ht->dds == NULL) {
        fprintf(stderr, "Unable to create DDS for the hybrid tuner");
        free(ht);
        return NULL;
    }

    return ht;
}
