//
//  hybrid_tuner_test.c
//  MSA
//
//  Created by William Dillon on 4/11/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <stdio.h>
#include "hybrid_tuner.h"

int main(void)
{
    // Scan through a range of tuning values starting at 1000 MHz and ending at
    // 2000 MHz.  Output these values as a comma-delimited list to easily import
    // into a spreadsheet.
    
    hybrid_tuner_t *ht = newHybridTuner(4118, 1);
    if (ht == NULL) {
        fprintf(stderr, "Unable to initialize the hybrid tuner.\n");
        return 1;
    }
    
    // Set the DDS ideal output frequency to 10.7 MHz
    HTsetIdealDDSFreq(ht, 10700000);
    // Set the PLL ideal phase frequency to 972 Khz
    HTsetIdealPhaseFreq(ht, 972000);
    // Set the reference clock to 64 MHz from the crystal oscillator
    HTsetRefClock(ht, 64000000);
    
    // Print the header
    printf("Req. Freq,Actual Freq,DDS Freq\n");
    
    // Iterate through the frequencies, generate 1000 samples
    int samples = 1000;
    double endFreq   = 2000000000.;
    double startFreq = 1000000000.;
    double increment = (endFreq - startFreq) / samples;
    
    for (int i = 0; i < samples; i++) {
        
        // Increment the requested frequency
        double requestedFreq = startFreq + increment * i;
        
        HTsetFrequency(ht, requestedFreq);
        
        // Print the results
        printf("%f,%f,%f\n", requestedFreq, ht->outFreq, ht->dds->outFreq);
    }
    
    return 0;
}