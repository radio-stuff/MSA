//
//  commander.h
//  MSA
//
//  Created by William Dillon on 4/12/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#ifndef MSA_commander_h
#define MSA_commander_h

// These functions handle the creation and management of the transition array
// used to command the MSA.  The design of these functions assumes that three
// periods are used to affect a complete SPI cycle.  Data words are changed
// when the clock is low, and the clock is not permitted to change states at
// the same time as any other pin.

// To begin using these functions, a transaction structure must be created.
// This structure maintains the states of each pin for a number of periods.

// It is possible to set the logic level of any pin to a set value by running
// the setPinState() function.  The logic level will remain that way until it's
// reset.  Pin state changes will be appended to the command stream.  This
// Allows the timing of pin state changes to be managed

// SPI traffic can be either inserted or appended to the transaction using the
// insertTransferSPI() and appendTransferSPI() functions, respectively.  The
// insert function takes an argument for the number of transitions within the
// command stream to insert the SPI transfer.  If you want the clocks to have
// the same phase, it's important to set this number appropriately.
// Clock, data, and latch pin numbers must be provided along with
// the length of the transfer and an integer containing the bits.  For now, a
// uint64_t is used and, therefore, the maximum SPI transfer is 64 bits.
// The append function simply inserts the SPI transfer into the stream at that
// point.

// After any of these functions have been used, the transition array will
// contain the data that can be used to produce the desired result.  The user
// must ensure that a newly created command_t type must be zeroed-out before
// use.  This is likely the case if it is allocated on the stack, but it may be
// necessary to do it explicitly if it's allocated on the heap.  For convenience
// functions to allocate and free the command structures are provided.  If a
// command is freed, it is vital to free the array to prevent a memory leak.

#include <stdint.h>

typedef struct {
    int length;
    uint32_t *array;
    int arrayLength;
} command_t;

command_t *newCommand(void);
void initPins(         uint32_t states);

void setPinState(      command_t *command, int pinNumber, int state);

void insertTransferSPI(command_t *command,
                       int clockPin, int dataPin, int latchPin,
                       int numBits, uint64_t data, int location);

void insertConvertADC( command_t *command,
                       int clockPin, int convertPin,
                       int numBits, int location);

void appendTransferSPI(command_t *command,
                       int clockPin, int dataPin, int latchPin,
                       int numBits, uint64_t data);

void freeCommand(      command_t *command);

#endif
