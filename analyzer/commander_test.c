//
//  commander_test.c
//  MSA
//
//  Created by William Dillon on 4/13/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <stdio.h>
#include "commander.h"

int main(void)
{
    command_t *command = newCommand();
    initPins(0x00000000);
//  setPinState(command, 24, 1);
    appendTransferSPI(command, 12, 16, 20, 16, 0xAA55);
    insertTransferSPI(command,  0,  4,  8, 16, 0x55AA, 0);
    
    for (int i = 0; i < command->length; i++) {
        printf("0x%08x\n", command->array[i]);
    }
    
    return 1;
}