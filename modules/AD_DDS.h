//
//  AD_DDS.h
//  Spectrum Analyzer
//
//  Created by William Dillon on 1/22/12.
//  Copyright (c) 2012. All rights reserved.
//

#ifndef AD_DDS_h
#define AD_DDS_h

#include <stdint.h>

// Users my access these values at any time, but it's not advisable to change them
// Changing these values through the functions below will ensure that they represent
// the actual values according to the hardware specifications.
typedef struct
{
    // Frequencies
    double refClock;
    double phase;
    double outFreq;
    
    // Chip options
    int powerDown;
    
    // Delta phase value
    uint32_t deltaPhase;
} DDS_module_t;

// These functions modify the state of the dds struct.  They will return the
// closest value to what the user requested.  They will also store this value
// within the struct.  To update the hardware, call generateRegisters().
double DDSsetFrequency(DDS_module_t *dds, double frequency);
double DDSsetRefClock( DDS_module_t *dds, double frequency);
double DDSsetPhase(    DDS_module_t *dds, double phase);
void   DDSsetpowerDown(DDS_module_t *dds, int    state);

// This function ensures that the module is setup correctly.
// This function will return an allocated dds struct.  This must be freed later.
// generateRegister MUST be run once and applied to the hardware for initialization.
DDS_module_t * newDDS(void);

// This function must be provided with an array of chars with space for 5 bytes.
// Its purpose is to take the commanded values and generate the values to be
// commanded into its registers.  They are in the order that they should appear
// in the wire.
uint64_t DDSgenerateRegisters(DDS_module_t *dds);

#endif
