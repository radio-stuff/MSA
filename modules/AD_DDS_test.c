//
//  AD_DDS_test.c
//  MSA
//
//  Created by William Dillon on 4/11/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <stdio.h>
#include "AD_DDS.h"

int main(void)
{
    // Scan through a range of tuning values starting at 1/1000 the reference
    // clock and ending at 1/2 the reference clock.  Output these values as a
    // comma-delimited list to easily import into a spreadsheet.
    
    DDS_module_t *dds = newDDS();
    if (dds == NULL) {
        fprintf(stderr, "Unable to initialize the DDS.\n");
        return 1;
    }
    
    // Set the reference clock to 64 MHz
    DDSsetRefClock(dds, 64000000);
    
    // Print the header
    printf("Req. Freq,Actual Freq,Delta phase\n");
    
    // Iterate through the frequencies, generate 1000 samples
    int samples = 1000;
    double startFreq = dds->refClock / 1000.;
    double increment = (dds->refClock - startFreq) / samples;
    uint64_t registerValue = 0;
    for (int i = 0; i < samples; i++) {
        // Increment the requested frequency
        double requestedFreq = startFreq + increment * i;

        DDSsetFrequency(dds, requestedFreq);
        
        // Print the results
        printf("%f,%f,%u\n", requestedFreq, dds->outFreq, dds->deltaPhase);
    }
    
    return 0;
}