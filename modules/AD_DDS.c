//
//  AD_DDS.m
//  Spectrum Analyzer
//
//  Created by William Dillon on 1/22/12.
//  Copyright (c) 2012. All rights reserved.
//

#include "AD_DDS.h"
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <math.h>

#define YES 1
#define NO  0

static uint8_t reverse[16] = {
    0x0, // 0000 -> 0000
    0x8, // 0001 -> 1000
    0x4, // 0010 -> 0100
    0xC, // 0011 -> 1100
    0x2, // 0100 -> 0010
    0xA, // 0101 -> 1010
    0x6, // 0110 -> 0110
    0xE, // 0111 -> 1110
    0x1, // 1000 -> 0001
    0x9, // 1001 -> 1001
    0x5, // 1010 -> 0101
    0xD, // 1011 -> 1101
    0x3, // 1100 -> 0011
    0xB, // 1101 -> 1011
    0x7, // 1110 -> 0111
    0xF, // 1111 -> 1111
};

#define RESERVED_WORD_MASK 0xFC

DDS_module_t * newDDS(void)
{
    // Allocate a structure
    DDS_module_t *dds = (DDS_module_t *)malloc(sizeof(DDS_module_t));
    if (dds == NULL) {
        perror("Unable to allocate memory for DDS module structure");
        return NULL;
    }
    
    // Setup the structure using defaults and input settings
    dds->powerDown = NO;

    // Use some reasonable defaults
    dds->refClock  = 64.;
    dds->phase     =  0.;
    dds->outFreq   = 10.7;
    
    return dds;
}

void DDSinitHardware(DDS_module_t *dds)
{
// TODO: Make this work somehow!
    // It should be safe to assume the pins are already set low.
    // Clock the parallel word once (loads serial mode into config)
//    [interface setPin:Clock Value:1];
//    [interface setPin:Clock Value:0];
    
    // Set FqUD high to latch into serial mode
//    [interface setPin:CS Value:1];
    
    // Now, the device is in serial mode, ready for loading
    // If, for some reason, the device was already in serial mode,
    // or is otherwise FUBAR'd, try to clear out the registers by
    // loading all 0's...
//    [interface sendSPIwithPin:Data
//                        Clock:Clock
//                           CS:CS
//                         data:0
//                         bits:40];
}

// This function should not be run by users
void DDSrecalculate(DDS_module_t *dds)
{
    // Calculate the delta phase value
    // This is the equation from the data sheet, but
    // re-arranged to solve for delta phase rather than Fout
    double floatPhase = (dds->outFreq * exp2(32)) / dds->refClock;
    if (floatPhase > UINT32_MAX) {
        fprintf(stderr, "The calculated delta phase is greater than uint32.\n");
    }
    
    dds->deltaPhase = (uint32_t)round(floatPhase);
    
    // Re-calculate the output frequency from the delta phase
    // This is the same equation that's in the datasheet
    dds->outFreq = ((double)dds->deltaPhase * dds->refClock) / exp2(32);
}

double DDSsetPhase(DDS_module_t *dds, double phase)
{
    if (dds == NULL) {
        fprintf(stderr, "DDS function called with NULL structure.\n");
        return -1;
    }
    
    dds->phase = phase;
    DDSrecalculate(dds);
    return dds->phase;
}

double DDSsetFrequency(DDS_module_t *dds, double frequency)
{
    if (dds == NULL) {
        fprintf(stderr, "DDS function called with NULL structure.\n");
        return -1;
    }

    dds->outFreq = frequency;
    DDSrecalculate(dds);
    return dds->outFreq;
}

double DDSsetRefClock(DDS_module_t *dds, double frequency)
{
    if (dds == NULL) {
        fprintf(stderr, "DDS function called with NULL structure.\n");
        return -1;
    }
    
    dds->refClock = frequency;
    DDSrecalculate(dds);
    return dds->refClock;
}

void DDSsetpowerDown(DDS_module_t *dds, int state)
{
    if (dds == NULL) {
        fprintf(stderr, "DDS function called with NULL structure.\n");
        return;
    }
    
    dds->powerDown = state;
    return;
}

uint64_t DDSgenerateRegisters(DDS_module_t *dds)
{
    if (dds == NULL) {
        fprintf(stderr, "DDS function called with NULL structure.\n");
        return 0;
    }
    
    uint64_t reg = dds->deltaPhase;

    // Because the DDS is loaded LSB-first,
    // we need to convert the register here.
    // To do this, we'll reverse each nibble,
    // then load them into a new register backwards.
    uint64_t reg2 = 0;
    
    for (int i = 0; i < 10; i++) {
        reg2  = reg2 << 4;
        reg2 |= reverse[(reg >> i*4) & 0xf];
    }
    
    // Set the powerdown pin?
    
    return reg;
}
