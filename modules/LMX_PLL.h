//
//  LMX_PLL.h
//  Spectrum Analyzer
//
//  Created by William Dillon on 1/21/12.
//  Copyright (c) 2012. All rights reserved.
//

#ifndef LMX_PLL_h
#define LMX_PLL_h

#include <stdint.h>

#define FoLD_TRIS 0 // Tri-state
#define FoLD_LDET 1 // Digital lock detect
#define FoLD_NDIV 2 // N Divider output
#define FoLD_LKAH 3 // Active HIGH
#define FoLD_RDIV 4 // R Divider output
#define FoLD_LKOD 5 // n Channel Open Drain
#define FoLD_SDAT 6 // Serial Data Out
#define FoLD_LKAL 7 // Active LOW

typedef struct {
    // LMX chip type
    int type;

    // Chip options
    int invertingCP;
    char FoLD_output;

    // Frequencies
    double refClock;
    double phaseFreq;
    double outFreq;

    // Don't change these!
    int initialized;
    int int_r_divider;
    int int_n_divider;
} LMX_module_t;

typedef struct {
    uint32_t rDividerRegister;
    uint32_t nDividerRegister;
    uint32_t functionRegister;
} LMX_registers_t;

// These functions modify the state of the lmx struct.  They will return the
// closest value to what the user requested.  They will also store this value
// within the struct.  To update the hardware, call generateRegisters().
double LMXsetFrequency( LMX_module_t *lmx, double frequency);
double LMXsetRefClock(  LMX_module_t *lmx, double frequency);
double LMXsetPhaseFreq( LMX_module_t *lmx, double frequency);
void   LMXsetFoLDOutput(LMX_module_t *lmx, int state);

// This function ensures that the module is setup correctly.
// This function will return an allocated lmx struct.  This must be freed later.
// generateRegister MUST be run once and applied to the hardware for initialization.
LMX_module_t * newLMXPLL(int type, int invertingCP);

// This function must be provided with an array of chars with space for 5 bytes.
// Its purpose is to take the commanded values and generate the values to be
// commanded into its registers.  They are in the order that they should appear
// in the wire.
void   LMXgenerateRegisters(LMX_module_t *lmx, LMX_registers_t *registers);

#endif
