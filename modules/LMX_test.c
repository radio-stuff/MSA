//
//  LMX_test.c
//  MSA
//
//  Created by William Dillon on 4/11/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <stdio.h>
#include "LMX_PLL.h"

int main(void)
{
    // Scan through a range of tuning values starting at 1000 MHz and ending at
    // 2000 MHz.  Output these values as a comma-delimited list to easily import
    // into a spreadsheet.
    
    LMX_module_t *lmx = newLMXPLL(4118, 0);
    if (lmx == NULL) {
        fprintf(stderr, "Unable to initialize the LMX.\n");
        return 1;
    }
    
    // Set the reference clock to 10.7 MHz
    LMXsetRefClock(lmx, 10700000);
    
    // Use a reasonable phase frequency
    LMXsetPhaseFreq(lmx, 972000);
    
    // Print the header
    printf("Req. Freq,Actual Freq,r divider,n divider\n");
    
    // Iterate through the frequencies, generate 1000 samples
    int samples = 1000;
    double endFreq   = 2000000000.;
    double startFreq = 1000000000.;
    double increment = (endFreq - startFreq) / samples;

    LMX_registers_t regs;
    
    for (int i = 0; i < samples; i++) {

        // Increment the requested frequency
        double requestedFreq = startFreq + increment * i;
        
        LMXsetFrequency(lmx, requestedFreq);
        
        // Print the results
        printf("%f,%f,%u,%u\n", requestedFreq, lmx->outFreq, lmx->int_r_divider, lmx->int_n_divider);
    }
    
    return 0;
}