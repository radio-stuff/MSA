//
//  LMX_PLL.m
//  Spectrum Analyzer
//
//  Created by William Dillon on 1/21/12.
//  Copyright (c) 2012. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include "LMX_PLL.h"

#define R_DIVIDER_MASK 0x3FFF
#define R_DIVIDER_REGISTER 0x0
#define A_COUNTER_MASK 0x1F
#define B_COUNTER_MASK 0x1FFF
#define N_DIVIDER_REGISTER 0x1

#define  FUNCTION_REGISTER 0x2
#define INITIALIZATION          (1 << 0)
#define COUNTER_RESET           (1 << 2)
#define POWER_DOWN              (1 << 3)
#define FOLD_CONTROL_OFFSET     4
#define FOLD_CONTROL_MASK       0x7
#define PD_POLARITY             (1 << 7)
#define CP_TRISTATE             (1 << 8)
#define FASTLOCK_ENABLE         (1 << 9)
#define FASTLOCK_CONTROL        (1 << 10)
#define TIMEOUT_ENABLE          (1 << 11)
#define TIMEOUT_COUNTER_OFFSET  12
#define TIMEOUT_COUNTER_MASK    0xF
#define POWER_DOWN_MODE         (1 << 19)

#define YES 1
#define NO  0

LMX_module_t * newLMXPLL(int type, int invertingCP)
{
    // Allocate a structure
    LMX_module_t *lmx = (LMX_module_t *)malloc(sizeof(LMX_module_t));
    if (lmx == NULL) {
        perror("Unable to allocate memory for LMX module structure");
        return NULL;
    }
    
    // Setup the structure using defaults and input settings
    lmx->type        = type;
    lmx->FoLD_output = FoLD_TRIS;
    lmx->invertingCP = invertingCP;
    lmx->initialized = NO;
    
    // Set these up so its obvious that we don't have settings for them yet.
    lmx->refClock  = -1.;
    lmx->phaseFreq = -1.;
    lmx->outFreq   = -1.;
    
    return lmx;
}

void LMXrecalculate(LMX_module_t *lmx)
{
    // Calculate the phase frequency
    double best_r_divider = lmx->refClock / lmx->phaseFreq;
    lmx->int_r_divider = (uint32_t)round(best_r_divider);
    lmx->phaseFreq = lmx->refClock / (double)lmx->int_r_divider;
    
    // Calculate the actual frequency
    double best_n_divider = lmx->outFreq / lmx->phaseFreq;
    lmx->int_n_divider = (uint32_t)round(best_n_divider);
    lmx->outFreq = (double)lmx->int_n_divider * lmx->phaseFreq;
}

double LMXsetRefClock(LMX_module_t *lmx, double frequency)
{
    if (lmx == NULL) {
        fprintf(stderr, "LMX function called with NULL structure.\n");
        return -1;
    }
    
    lmx->refClock = frequency;
    LMXrecalculate(lmx);
    return lmx->refClock;
}

double LMXsetPhaseFreq( LMX_module_t *lmx, double frequency)
{
    if (lmx == NULL) {
        fprintf(stderr, "LMX function called with NULL structure.\n");
        return -1;
    }
    
    lmx->phaseFreq = frequency;
    LMXrecalculate(lmx);
    return lmx->phaseFreq;
}

double LMXsetFrequency( LMX_module_t *lmx, double frequency)
{
    if (lmx == NULL) {
        fprintf(stderr, "LMX function called with NULL structure.\n");
        return -1;
    }
    
    lmx->outFreq = frequency;
    LMXrecalculate(lmx);
    return lmx->outFreq;
}

void LMXsetFoLDOutput(LMX_module_t *lmx, int state)
{
    if (lmx == NULL) {
        fprintf(stderr, "LMX function called with NULL structure.\n");
        return;
    }
    
    lmx->FoLD_output = state;
    
    return;
}

void LMXgenerateRegisters(LMX_module_t *lmx, LMX_registers_t *registers)
{
    if (lmx == NULL) {
        fprintf(stderr, "LMX function called with NULL structure.\n");
        return;
    }
    
    uint32_t prescaler;
    switch (lmx->type) {
        case 2306:
            prescaler = 8;
            break;
            
        default:
            prescaler = 32;
            break;
    }
    
    uint32_t int_b_divider = lmx->int_n_divider / prescaler;
    uint32_t int_a_divider = lmx->int_n_divider % prescaler;
    
    // Generate register values
    // The left-shift is for the control bits
    registers->rDividerRegister  = (lmx->int_r_divider & R_DIVIDER_MASK) << 2;
    registers->rDividerRegister |= R_DIVIDER_REGISTER;
    
    // The N divider is made up of the A and B dividers
    registers->nDividerRegister  = (int_b_divider & B_COUNTER_MASK) << 7;
    registers->nDividerRegister |= (int_a_divider & A_COUNTER_MASK) << 2;
    registers->nDividerRegister |= N_DIVIDER_REGISTER;
    
    // The setting of the function reguster takes into account initialization
    registers->functionRegister  = (lmx->invertingCP)? 0 : PD_POLARITY;
    registers->functionRegister |= (lmx->FoLD_output & FOLD_CONTROL_MASK) << FOLD_CONTROL_OFFSET;
    registers->functionRegister |= (lmx->initialized == NO)? INITIALIZATION : 0;
    registers->functionRegister |= FUNCTION_REGISTER;
    lmx->initialized = YES;

    return;
}
