//
//  analyzer.c
//  MSA
//
//  Created by William Dillon on 4/17/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "analyzer.h"
#include "commander.h"

#ifdef STANDARD_MSA

#define PLO1_CLOCK   0
#define PLO1_DATA    1
#define PLO1_LATCH   8
#define PLO2_CLOCK   0
#define PLO2_DATA    4
#define PLO2_LATCH  12
#define PLO3_CLOCK   0
#define PLO3_DATA    3
#define PLO3_LATCH  10
#define DDS1_CLOCK   0
#define DDS1_DATA    2
#define DDS1_LATCH   9
#define DDS3_CLOCK   0
#define DDS3_DATA    4
#define DDS3_LATCH  11
#define PDM_INVERT  14
#define ADC_CLOCK   22
#define ADC_CONV    23
#define FILT_A0      5
#define FILT_A1      6
#define VFILT_V0    24
#define VFILT_V1    25
#define RF_G0       26
#define RF_G1       27
#define FWD_REV     28
#define TRANS_REF   28
#define RLY_LATCH   31

#else

#define PLO1_CLOCK   0
#define PLO1_DATA    1
#define PLO1_LATCH   2
#define PLO2_CLOCK  16
#define PLO2_DATA   17
#define PLO2_LATCH  18
#define PLO3_CLOCK   8
#define PLO3_DATA    9
#define PLO3_LATCH  10
#define DDS1_CLOCK   3
#define DDS1_DATA    4
#define DDS1_LATCH   5
#define DDS3_CLOCK  11
#define DDS3_DATA   12
#define DDS3_LATCH  13
#define PDM_INVERT  14
#define ADC_CLOCK   19
#define ADC_CONV    20
#define FILT_A0      6
#define FILT_A1      7
#define VFILT_V0    24
#define VFILT_V1    25
#define RF_G0       26
#define RF_G1       27
#define FWD_REV     28
#define TRANS_REF   28
#define RLY_LATCH   31

#endif

analyzer_t *newAnalyzer(driver_t *driver)
{
    // Create the struct memory
    analyzer_t *analyzer = (analyzer_t *)malloc(sizeof(analyzer_t));
    
    // Create and initialize the components
    analyzer->lo2pll  = newLMXPLL(4118, 0);
    
    analyzer->tuner   = newHybridTuner(4118, 1);
    HTsetRefClock(      analyzer->tuner, 64000000);
    HTsetIdealPhaseFreq(analyzer->tuner,   972000);
    HTsetIdealDDSFreq(  analyzer->tuner, 10700000);
    
    analyzer->tracker = newHybridTuner(4118, 1);
    HTsetRefClock(      analyzer->tracker, 64000000);
    HTsetIdealPhaseFreq(analyzer->tracker,   972000);
    HTsetIdealDDSFreq(  analyzer->tracker, 10700000);

    // Setup the LO2 pll
    LMXsetRefClock( analyzer->lo2pll,   64000000);
    LMXsetPhaseFreq(analyzer->lo2pll,    8000000);
    LMXsetFrequency(analyzer->lo2pll, 1024000000);
    
    // Tune 0Hz (1013.7 for hybrid tuner)
    HTsetFrequency(analyzer->tuner,   1013700000);
    HTsetFrequency(analyzer->tracker, 1013700000);
    
    // Perform the initialization for the devices
    LMX_registers_t plo1registers;
    LMX_registers_t plo2registers;
    LMX_registers_t plo3registers;
    uint64_t dds1register, dds3register;
    
    // Generate all the register commands for initialization
    LMXgenerateRegisters(analyzer->tuner->pll,   &plo1registers);
    LMXgenerateRegisters(analyzer->tracker->pll, &plo2registers);
    LMXgenerateRegisters(analyzer->lo2pll,       &plo3registers);
    dds1register = DDSgenerateRegisters(analyzer->tuner->dds);
    dds3register = DDSgenerateRegisters(analyzer->tracker->dds);
    
    // Create a command stream for initialization
    // The init pins command will set initial states for all the CB pins
    // This is a convenient time to set filters and stuff.
    initPins(0x00000000);
    command_t *command = newCommand();
    
    // Make both DDS1 and DDS3 come up in serial mode.  This is done by clocking
    // a virtual one-bit SPI transfer.
    appendTransferSPI(command, DDS1_CLOCK, DDS1_DATA, DDS1_LATCH, 1, 0);
    insertTransferSPI(command, DDS1_CLOCK, DDS1_DATA, DDS1_LATCH, 1, 0, 0);
    driver->command(driver, command->length, command->array);
    freeCommand(command);
    
    command = newCommand();
    // Detect whether pins are being overloaded (as in the MSA standard wiring)
    if (PLO2_DATA == DDS3_DATA) {
        // Because the DDS3 and PLO2 data pins are the same, we need to command
        // them seperately. So, we'll command PLO2 first, then once it's
        // finished, we'll command DDS3.  Everything else can complete in
        // parallel.
        appendTransferSPI(command, PLO2_CLOCK, PLO2_DATA, PLO2_LATCH, 21, plo2registers.nDividerRegister);
        appendTransferSPI(command, PLO2_CLOCK, PLO2_DATA, PLO2_LATCH, 21, plo2registers.rDividerRegister);
        appendTransferSPI(command, PLO2_CLOCK, PLO2_DATA, PLO2_LATCH, 21, plo2registers.functionRegister);
        appendTransferSPI(command, DDS3_CLOCK, DDS3_DATA, DDS3_LATCH, 40, dds3register);
        insertTransferSPI(command, DDS1_CLOCK, DDS1_DATA, DDS1_LATCH, 40, dds1register, 0);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.nDividerRegister, 0);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.rDividerRegister, 21 * 3 + 2);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.functionRegister, 42 * 3 + 4);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.nDividerRegister, 0);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.rDividerRegister, 21 * 3 + 2);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.functionRegister, 42 * 3 + 4);
    } else {
        appendTransferSPI(command, PLO2_CLOCK, PLO2_DATA, PLO2_LATCH, 21, plo2registers.nDividerRegister);
        appendTransferSPI(command, PLO2_CLOCK, PLO2_DATA, PLO2_LATCH, 21, plo2registers.rDividerRegister);
        appendTransferSPI(command, PLO2_CLOCK, PLO2_DATA, PLO2_LATCH, 21, plo2registers.functionRegister);
        insertTransferSPI(command, DDS1_CLOCK, DDS1_DATA, DDS1_LATCH, 40, dds1register, 0);
        insertTransferSPI(command, DDS3_CLOCK, DDS3_DATA, DDS3_LATCH, 40, dds3register, 0);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.nDividerRegister, 0);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.rDividerRegister, 21 * 3 + 2);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.functionRegister, 42 * 3 + 4);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.nDividerRegister, 0);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.rDividerRegister, 21 * 3 + 2);
        insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, plo1registers.functionRegister, 42 * 3 + 4);
    }

    // Command the MSA with the initialization values
    driver->command(driver, command->length, command->array);
    
    return analyzer;
}

double setTunerFreq(analyzer_t *analyzer, double frequency)
{
    // Compute the tuning value and registers
    double actualTuning = HTsetFrequency(analyzer->tuner, frequency);
    
    // We need to retune PLO1 and DDS1
    LMX_registers_t ploRegisters;
    uint64_t ddsRegister;
    
    LMXgenerateRegisters(analyzer->tuner->pll, &ploRegisters);
    ddsRegister = DDSgenerateRegisters(analyzer->tuner->dds);
    
    // Create a new command for this tuning
    command_t *command = newCommand();
    appendTransferSPI(command, DDS1_CLOCK, DDS1_DATA, DDS1_LATCH, 40, ddsRegister);
    insertTransferSPI(command, PLO1_CLOCK, PLO1_DATA, PLO1_LATCH, 21, ploRegisters.nDividerRegister, 0);
    
    // Command the MSA
    analyzer->driver->command(analyzer->driver, command->length, command->array);
    
    // Return the actual frequency
    return actualTuning;
}

double setTrackerFreq(analyzer_t *analyzer, double frequency)
{
    // Compute the tuning value and registers
    double actualTuning = HTsetFrequency(analyzer->tracker, frequency);
    
    // We need to retune PLO3 and DDS3
    LMX_registers_t ploRegisters;
    uint64_t ddsRegister;
    
    LMXgenerateRegisters(analyzer->tuner->pll, &ploRegisters);
    ddsRegister = DDSgenerateRegisters(analyzer->tuner->dds);
    
    // Create a new command for this tuning
    command_t *command = newCommand();
    appendTransferSPI(command, DDS3_CLOCK, DDS3_DATA, DDS3_LATCH, 40, ddsRegister);
    insertTransferSPI(command, PLO3_CLOCK, PLO3_DATA, PLO3_LATCH, 21, ploRegisters.nDividerRegister, 0);
    
    // Command the MSA
    analyzer->driver->command(analyzer->driver, command->length, command->array);
    
    // Return the actual frequency
    return actualTuning;
}

// When taken alone, this function has to create a new command set.
// When performing a scan, this can be combined with other operations.
void getADCValues(analyzer_t *analyzer, double *magnitude, double *phase)
{
    
}
