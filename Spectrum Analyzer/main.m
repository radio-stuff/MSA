//
//  main.m
//  Spectrum Analyzer
//
//  Created by William Dillon on 4/11/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
