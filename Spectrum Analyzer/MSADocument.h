//
//  MSADocument.h
//  Spectrum Analyzer
//
//  Created by William Dillon on 4/11/13.
//  Copyright (c) 2013 William Dillon. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "analyzer.h"

@interface MSADocument : NSDocument
{
    analyzer_t *analyzer;
}


@end
